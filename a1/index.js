const firstName = "John";
const firstNameDesc = "First Name: " + firstName;
console.log(firstNameDesc);

const lastName = "Smith";
const lastNameDesc = "Last Name: " + lastName;
console.log(lastNameDesc);

const age = 30;
const ageDesc = "Age: " + age;
console.log(ageDesc);

const hobbyDesc = "Hobbies:";
console.log(hobbyDesc);

const hobby = ["Biking", "Mountain Climbing", "Swimming"];
console.log(hobby);

const address = "Work Address:";
console.log(address);

const addressDetails = {
    houseNumber: 32,
    street: "Washington",
    city: "Lincoln",
    state: "Nebraska",
};
console.log(addressDetails);

const fullNameSteve = "Steve Rogers";
const introduction = "My full name is: " + fullNameSteve;
console.log(introduction);

const steveRogerAge = 40;
const steveRogerIntroduction = "My current age is: " + steveRogerAge;
console.log(steveRogerIntroduction);

const friends = "My friends are: ";
console.log(friends);

const steveRogerFriends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log(steveRogerFriends);

const profile = "My full Profile: ";
console.log(profile);

const steveRogerProfile = {
    userName: "captain_america",
    fullName: fullNameSteve,
    age: steveRogerAge,
    isActive: false,
};
console.log(steveRogerProfile);

const bestFriendName = "Bucky Barnes";
const bestFriend = "My bestFriend is: " + bestFriendName;
console.log(bestFriend);

const locationOfSteve = "Arctic Ocean";
const foundLocation = "I was found frozen in: " + locationOfSteve;
console.log(foundLocation);
