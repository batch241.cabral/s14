console.log("Hello World");

let myVariable = "Ada Lovelace";
let variable;
console.log(variable);
console.log(myVariable);

const constVariable = "John Doe";

let productName = "desktop computer";
console.log(productName);

let desktopName;
console.log(desktopName);

desktopName = "Dell";
console.log(desktopName);

productName = "personal computer";
console.log("personal computer");

const name = "erven";
console.log(name);

// name = "ej";
// console.log(name);

// let productName = 'Laptop'

let outerVariable = "hello";
{
    let innerVariable = "hello again";
    console.log(innerVariable);
}

console.log(outerVariable);

// multiple variable declaration

let productCode = 23232;
const productBrand = "DELL";
console.log(typeof productCode);
console.log(productBrand);

let country = "Philippines";
let province = "Metro Manila";
console.log(country);
console.log(province);

let fullAddress = province + "," + country;
console.log(fullAddress);

let message = "John's em";
console.log(typeof message);

const palindromeFuntion = (stringInput) => {
    return stringInput.split("").reverse().join("");
};
console.log(palindromeFuntion("emordnilap si sihT"));

const simpleArray = [1, 2, 3, 4];
simpleArray.push(5);

console.log(simpleArray);

console.log((simpleArray[simpleArray.length] = 6));

console.log(simpleArray.length);

const arrayOfOjects = [
    {
        market: 20,
        trial: 21,
        mix: 25,
    },
    {
        market: 20,
        trial: 21,
        mix: 25,
    },
];
console.log(arrayOfOjects);
console.log(...simpleArray, ...arrayOfOjects);

let person = {
    firstName: "Erven Joshua",
    lastName: "Cabral",
    age: 23,
    contact: [090990, 090990],
    address: {
        address1: "address1",
        address2: "address2",
    },
};
console.log(person);

let myGrades = {
    firstGrading: 98,
    secondGrading: 92.1,
    thirdGrading: 90.1,
    fourthGrading: 94.7,
};
console.log(myGrades);

const anime = ["one piece", "one punch man", "your lie in april"];
anime.push("max");
console.log(anime);

anime[0] = "kimetsu no yaba";
console.log(anime);

let fullName;
console.log(fullName);
